import { createRouter, createWebHashHistory } from "vue-router";

import HomeView from "../views/HomeView2.vue";
import AboutView from "../views/AboutView.vue";
import LoginView from "../views/LoginView.vue";
import BuildingsView from "../views/BuildingsView.vue";
import RoomsView from "../views/RoomsView.vue";
import AllRoomsView from "../views/AllRoomsView.vue";
import ObservationsView from "../views/ObservationsView.vue";

const routes = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "/about",
    name: "about",
    component: AboutView,
  },
  {
    path: "/Login",
    name: "login",
    component: LoginView,
  },
  {
    path: "/rooms/:id",
    name: "rooms",
    component: RoomsView,
  },
  {
    path: "/allRooms",
    name: "allRooms",
    component: AllRoomsView,
  },
  {
    path: "/buildings",
    name: "building",
    component: BuildingsView,
  },
  {
    path: "/room/:id",
    name: "room",
    component: ObservationsView,
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
