import { createApp } from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import BootstrapVue from 'bootstrap-vue-3';
import { Toaster } from '@meforma/vue-toaster'
import 'bootstrap-vue/dist/bootstrap-vue.css';
import 'bootstrap/dist/css/bootstrap.css';

const app = createApp(App)
app.use(store)
app.use(router)
app.use(BootstrapVue)
app.use(Toaster)


router.isReady().then(()=>{
    app.mount('#app')
})